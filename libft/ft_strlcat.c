/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 13:20:06 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 08:36:21 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	unsigned long	sl;
	unsigned long	dl;
	unsigned long	i;

	i = 0;
	while (src[i])
		i++;
	sl = i;
	i = 0;
	while (dest[i])
		i++;
	dl = i;
	i = 0;
	while (src[i] != '\0' && ((dl + i) < (size - 1)))
	{
		dest[dl + i] = src[i];
		i++;
	}
	dest[dl + i] = '\0';
	if ((size_t)dl < size)
		return ((size_t)(sl + dl));
	else
		return ((size_t)(sl + size));
}
