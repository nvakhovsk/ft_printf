/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 13:15:05 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 13:18:38 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *s1, const char *s2)
{
	int	res;
	int	i;

	i = -1;
	res = 0;
	while (s1[++i] != '\0' && !(res))
		res = (int)((unsigned char)s1[i] - (unsigned char)s2[i]);
	if (res)
		return (res);
	else
		res = (int)((unsigned char)s1[i] - (unsigned char)s2[i]);
	return (res);
}
