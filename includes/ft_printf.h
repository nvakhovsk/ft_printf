/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 10:09:05 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:35:57 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <wchar.h>
# include "../libft/libft.h"

/*
** size_flag
** 1 - hh
** 2 - h
** 3 - l
** 4 - ll
** 5 - j
** 6 - z
*/

typedef struct			s_print
{
	int					octothorp;
	int					min;
	int					null;
	int					plus;
	int					space;
	int					width;
	int					precis;
	int					size;
	char				spec;
	int					sz_fl;

}						t_print;

char					*ft_print_p_width(t_print *print, char *s);
char					*ft_check_wstring(char *s, char *t);
char					*ft_check_string(t_print *print, char *s, char *t);
char					*ft_print_p_precis(t_print *print, char *s);
char					*ft_ui_to_str(unsigned long long ui, int b);

char					*ft_conv_gen(int i, wint_t c);
char					*ft_conv2(wint_t c);
char					*ft_convert_to_str(wchar_t *s, int ml);

void					ft_print_a_ch(t_print *print, va_list arguments,
										int *res);
void					ft_print_a_poiner(t_print *print, va_list arguments,
											int *res);

void					ft_print_a_numbers(t_print *print, va_list a,
											int *res);
char					*ft_print_b(t_print *print, va_list arguments);
char					*ft_print_x(t_print *print, va_list arguments);
char					*ft_print_o(t_print *print, va_list arguments);
char					*ft_print_u(t_print *print, va_list arguments);

char					*ft_string_pecis(t_print *print, char *s);
void					ft_print_a_string(t_print *print, va_list a, int *res);
char					*ft_for_string(char *s1, char *s2, int flag);
char					*ft_string_width(t_print *print, char *s);

char					*ft_print_id(t_print *print, va_list arguments);
long long				ft_idn(t_print *print, va_list a);
char					*ft_n_fl(t_print *print, char *s);
unsigned long long		ft_un(t_print *print, va_list a);
char					*ft_s_tolower(char *t);

int						ft_check_flag(int r, const char *f);
void					helper(va_list arguments, const char *format, int *f);
int						ft_init_and_parse(va_list arguments, t_print *print,
							const char *format, int res);
int						ft_print_arguments(va_list arguments, t_print *print);
int						ft_printf(const char *format, ...);

void					ft_parse_pr_size(t_print *print, const char *f,
										va_list arguments, int *r);
void					ft_parse_specifier(t_print *print, const char *f,
											int *r);
int						ft_p_size(int *r, const char *f);
void					ft_p_flag(t_print *print, int *r, const char *f);
void					ft_parse_fl_width(t_print *print, const char *f,
									va_list a, int *r);

#endif
