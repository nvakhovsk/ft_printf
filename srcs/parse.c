/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:58:02 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:05:46 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	ft_parse_pr_size(t_print *print, const char *f, va_list arguments,
							int *r)
{
	int	res;

	if (f[*r] == '.')
	{
		*r += 1;
		print->precis = 0;
		if (ft_isdigit(f[*r]))
		{
			print->precis = ft_atoi(&f[*r]);
			while (ft_isdigit(f[*r]))
				*r += 1;
		}
		else if (f[*r] == '*')
		{
			print->precis = va_arg(arguments, int);
			*r += 1;
		}
	}
	while (ft_check_flag(*r, f))
	{
		res = ft_p_size(r, f);
		if (print->sz_fl < res)
			print->sz_fl = res;
	}
}

void	ft_parse_specifier(t_print *print, const char *f, int *r)
{
	char	t;

	t = f[*r];
	if (t == 'D' || t == 'O' || t == 'U' || t == 'C' || t == 'S')
	{
		print->sz_fl = 3;
		t = (char)ft_tolower(t);
	}
	print->spec = t;
}

int		ft_p_size(int *r, const char *f)
{
	int		res;

	res = 0;
	if (f[*r] == 'h' && f[*r + 1] == 'h')
		res = 1;
	else if (f[*r] == 'h')
		res = 2;
	else if (f[*r] == 'l')
		res = 3;
	else if (f[*r] == 'l' && f[*r + 1] == 'l')
		res = 4;
	else if (f[*r] == 'j')
		res = 5;
	else if (f[*r] == 'z')
		res = 6;
	if ((f[*r] == 'h' && f[*r + 1] == 'h') ||
			(f[*r] == 'l' && f[*r + 1] == 'l'))
		*r += 2;
	else
		*r += 1;
	return (res);
}

void	ft_p_flag(t_print *print, int *r, const char *f)
{
	if (f[*r] == ' ' && !print->plus)
		print->space = 1;
	else if (f[*r] == '#')
		print->octothorp = 1;
	else if (f[*r] == '0' && !print->min)
		print->null = 1;
	else if (f[*r] == '-')
	{
		print->min = 1;
		print->null = 0;
	}
	else if (f[*r] == '+')
	{
		print->plus = 1;
		print->space = 0;
	}
	*r += 1;
}

void	ft_parse_fl_width(t_print *print, const char *f, va_list a, int *r)
{
	while (f[*r] == ' ' || f[*r] == '#' || f[*r] == '0' || f[*r] == '-' ||
			f[*r] == '+')
		ft_p_flag(print, r, f);
	if (ft_isdigit(f[*r]))
	{
		print->width = ft_atoi(f + *r);
		while (ft_isdigit(f[*r]))
			*r += 1;
	}
	else
	{
		if (f[*r] == '*')
		{
			if ((print->width = va_arg(a, int)) < 0)
			{
				print->min = 1;
				print->null = 0;
				print->width *= -1;
			}
			*r += 1;
		}
	}
}
