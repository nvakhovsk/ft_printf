/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_a_string.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:52:50 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:18:00 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char			*ft_string_pecis(t_print *print, char *s)
{
	char	*t;
	int		i;

	i = -1;
	if (print->precis >= 0)
	{
		if ((print->precis >= 0) && (print->precis < (int)ft_strlen(s)))
		{
			t = ft_strnew(print->precis);
			while (++i < print->precis)
				t[i] = s[i];
			t[i] = '\0';
			return (t);
		}
	}
	return (s);
}

void			ft_print_a_string(t_print *print, va_list a, int *res)
{
	char	*s;

	if (print->sz_fl == 3)
		s = ft_convert_to_str(va_arg(a, wchar_t *), print->precis);
	else
	{
		s = va_arg(a, char *);
		s = s ? ft_strdup(s) : ft_strdup("(null)");
	}
	s = ft_string_pecis(print, s);
	s = ft_string_width(print, s);
	if (s)
		*res += write(1, s, ft_strlen(s));
	free(s);
}

char			*ft_for_string(char *s1, char *s2, int flag)
{
	char	*t;
	int		i;
	int		j;
	int		l;

	i = -1;
	l = ft_strlen(s1) + ft_strlen(s2);
	if (s1 && s2 && (t = (char *)malloc(sizeof(char) * (l + 1))))
	{
		while (s1[++i])
			t[i] = s1[i];
		j = -1;
		while (s2[++j])
			t[i++] = s2[j];
		t[i] = '\0';
		(flag == 1 || flag == 0) ? free(s1) : 0;
		(flag == 2 || flag == 0) ? free(s2) : 0;
		return (t);
	}
	return (NULL);
}

char			*ft_string_width(t_print *print, char *s)
{
	char	*t;
	int		i;

	i = -1;
	if (print->width > 0)
	{
		if (print->width > (int)ft_strlen(s) && print->width > 0)
		{
			print->width -= (int)ft_strlen(s);
			t = ft_strnew(print->width);
			while (++i < print->width)
				t[i] = print->null ? '0' : ' ';
			t[i] = '\0';
			s = print->min == 1 ? ft_for_string(s, t, 1) :
									ft_for_string(t, s, 2);
			free(t);
		}
	}
	return (s);
}
