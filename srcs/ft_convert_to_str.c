/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_to_str.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:55:48 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:25:21 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void	helper2(char **res, char c)
{
	char	*wtmp;

	wtmp = ft_conv2(c);
	*res = ft_strjoin(*res, wtmp);
	free(wtmp);
}

char		*ft_conv_gen(int i, wint_t c)
{
	char	*s;

	s = (char *)malloc(sizeof(char) * (i + 1));
	s[i--] = '\0';
	if (i == 0)
	{
		s[i] = (char)c;
		return (s);
	}
	s[i--] = (char)((c & 63) + 128);
	if (i == 0)
	{
		s[i] = (char)(((c & 1984) >> 6) + 192);
		return (s);
	}
	s[i--] = (char)(((c & 4032) >> 6) + 128);
	if (i == 0)
	{
		s[i] = (char)(((c & 61440) >> 12) + 224);
		return (s);
	}
	s[i--] = (char)(((c & 258048) >> 12) + 128);
	s[i] = (char)(((c & 1835008) >> 18) + 240);
	return (s);
}

char		*ft_conv2(wint_t c)
{
	if (c < 128)
		return (ft_conv_gen(1, c));
	if (c < 2048)
		return (ft_conv_gen(2, c));
	if (c < 65536)
		return (ft_conv_gen(3, c));
	if (c < 1114112)
		return (ft_conv_gen(4, c));
	return (NULL);
}

char		*ft_convert_to_str(wchar_t *s, int ml)
{
	int		l;
	char	*res;
	char	*temp;

	if (s)
	{
		res = ft_strdup("");
		l = -1;
		while (s[++l])
		{
			temp = res;
			helper2(&res, s[l]);
			if (ml >= 0 && (int)ft_strlen(res) > ml)
			{
				free(res);
				res = temp;
				break ;
			}
			free(temp);
		}
		return (res);
	}
	return (ft_strdup("(null)"));
}
