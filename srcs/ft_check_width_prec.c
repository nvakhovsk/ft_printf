/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_width_prec.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:48:00 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:38:31 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char	*ft_print_p_width(t_print *print, char *s)
{
	char	*t;
	char	*result;

	if (print->width > (int)ft_strlen(s) && print->width > 0)
	{
		print->width -= (int)ft_strlen(s);
		t = ft_strnew(print->width);
		t = (print->precis < 0 && print->null) ?
			ft_memset(t, '0', print->width) : ft_memset(t, ' ', print->width);
		if (print->min || print->null)
			result = (print->null) ? ft_check_wstring(s, t) : ft_strjoin(s, t);
		else
			result = ft_strjoin(t, s);
		free(t);
		free(s);
		return (result);
	}
	return (s);
}

char	*ft_check_wstring(char *s, char *t)
{
	char *str;

	if (s[0] == ' ' || s[0] == '-' || s[0] == '+')
	{
		if (s[0] == ' ')
			str = ft_for_string(ft_strjoin(" ", t), s + 1, 1);
		else
			str = ft_for_string(ft_strjoin((s[0] == '-' ? "-" : "+"), t),
											s + 1, 1);
	}
	else if (s[1] == 'x' || s[1] == 'X' || s[1] == 'b')
	{
		if (s[1] == 'x')
			str = ft_for_string(ft_strjoin("0x", t), s + 2, 1);
		else
			str = ft_for_string(ft_strjoin((s[1] == 'X' ? "0X" : "0b"), t),
											s + 2, 1);
	}
	else
		str = ft_strjoin(t, s);
	return (str);
}

char	*ft_check_string(t_print *print, char *s, char *t)
{
	char	*str;

	if (s[0] == '-')
		str = ft_for_string(ft_strjoin("-", t), s + 1, 1);
	else if (print->spec == 'o' && print->octothorp == '#')
		str = ft_strjoin(t, s + 1);
	else if (s[1] == 'x' || s[1] == 'X' || s[1] == 'b')
	{
		if (s[1] == 'x' || s[1] == 'X')
			str = s[1] == 'x' ? ft_for_string(ft_strjoin("0x", t), s + 2, 1) :
							ft_for_string(ft_strjoin("0X", t), s + 2, 1);
		else
			str = ft_for_string(ft_strjoin("0b", t), s + 2, 1);
	}
	else
		str = ft_strjoin(t, s);
	return (str);
}

char	*ft_print_p_precis(t_print *print, char *s)
{
	char	*t;
	char	*result;
	int		l;
	int		ml;
	int		i;

	l = ft_strlen(s);
	if (s[1] == 'b' || s[1] == 'x' || s[1] == 'X' || s[0] == '-' ||
		(print->spec == 'o' && print->octothorp))
		l = (s[1] == 'b' || s[1] == 'x' || s[1] == 'X') ? ft_strlen(s) - 2 :
			ft_strlen(s) - 1;
	i = -1;
	if (print->precis > l)
	{
		ml = print->precis - l;
		t = ft_strnew(ml + 1);
		while (++i < ml)
			t[i] = '0';
		t[i] = '\0';
		result = ft_check_string(print, s, t);
		free(t);
		free(s);
		return (result);
	}
	return (s);
}

char	*ft_ui_to_str(unsigned long long ui, int b)
{
	int					i;
	unsigned long long	t;
	char				*s;
	char				*barr;

	i = 1;
	t = ui;
	while (ui /= b)
		++i;
	barr = "0123456789ABCDEF";
	if ((s = (char *)malloc(sizeof(char) * (i + 1))))
	{
		s[i] = '\0';
		while (--i >= 0)
		{
			s[i] = barr[t % b];
			t /= b;
		}
		return (s);
	}
	return (NULL);
}
