/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 10:06:37 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:39:14 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		ft_check_flag(int r, const char *f)
{
	if ((f[r] == 'h' && f[r + 1] == 'h') || f[r] == 'h' || f[r] == 'l' ||
			(f[r] == 'l' && f[r + 1] == 'l') || f[r] == 'j' || f[r] == 'z')
		return (1);
	return (0);
}

void	helper(va_list arguments, const char *format, int *f)
{
	int			i;
	t_print		*print;

	i = -1;
	while (format[++i])
	{
		if (format[i] == '%')
		{
			print = (t_print *)malloc(sizeof(t_print));
			i = ft_init_and_parse(arguments, print, format, i + 1);
			if (format[i] == '\0')
				break ;
			*f += ft_print_arguments(arguments, print);
			free(print);
		}
		else
			*f += write(1, &format[i], 1);
	}
}

int		ft_init_and_parse(va_list arguments, t_print *print,
							const char *format, int res)
{
	int	t;

	t = 0;
	print->octothorp = 0;
	print->min = 0;
	print->null = 0;
	print->plus = 0;
	print->space = 0;
	print->width = 0;
	print->precis = -1;
	print->size = 0;
	print->sz_fl = 0;
	print->spec = '\0';
	while (res > t && format[res])
	{
		t = res;
		ft_parse_fl_width(print, format, arguments, &res);
		ft_parse_pr_size(print, format, arguments, &res);
	}
	if (format[res])
		ft_parse_specifier(print, format, &res);
	return (res);
}

int		ft_print_arguments(va_list arguments, t_print *print)
{
	int res;

	res = 0;
	if (print->spec == 's')
		ft_print_a_string(print, arguments, &res);
	else if (print->spec == 'p')
		ft_print_a_poiner(print, arguments, &res);
	else if (ft_strchr("idouXxb", print->spec))
		ft_print_a_numbers(print, arguments, &res);
	else
		ft_print_a_ch(print, arguments, &res);
	return (res);
}

int		ft_printf(const char *format, ...)
{
	va_list		arguments;
	int			flag;

	flag = 0;
	if (format)
	{
		va_start(arguments, format);
		helper(arguments, format, &flag);
		va_end(arguments);
		return (flag);
	}
	return (-1);
}
