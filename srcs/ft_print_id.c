/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_id.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:39:44 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 14:37:49 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char					*ft_print_id(t_print *print, va_list arguments)
{
	char		*t;
	char		*s;
	long long	n;
	int			flag;

	n = ft_idn(print, arguments);
	flag = 0;
	n *= n < 0 && (flag = 1) ? -1 : 1;
	t = ft_ui_to_str((unsigned long long)n, 10);
	s = flag ? ft_for_string("-", t, 2) : t;
	if (!n && print->precis == 0)
	{
		free(s);
		s = ft_strdup("");
	}
	s = ft_print_p_precis(print, s);
	s = ft_n_fl(print, s);
	s = ft_print_p_width(print, s);
	return (s);
}

long long				ft_idn(t_print *print, va_list a)
{
	long long int		n;

	if (print->sz_fl == 1 || print->sz_fl == 2)
		n = print->sz_fl == 1 ? (signed char)va_arg(a, int) :
									(short)va_arg(a, int);
	else if (print->sz_fl == 3 || print->sz_fl == 4)
		n = print->sz_fl == 3 ? (long)va_arg(a, long int) :
					(long long)va_arg(a, long long int);
	else if (print->sz_fl == 5 || print->sz_fl == 6)
		n = print->sz_fl == 5 ? (uintmax_t)va_arg(a, uintmax_t) :
								(size_t)va_arg(a, size_t);
	else
		n = (int)va_arg(a, int);
	return (n);
}

char					*ft_n_fl(t_print *print, char *s)
{
	char	*t;

	if ((print->plus && !ft_strchr(s, '-')) ||
		(print->space && !ft_strchr(s, '-')))
	{
		t = print->plus ? ft_strjoin("+", s) : ft_strjoin(" ", s);
		free(s);
	}
	else
		t = s;
	return (t);
}

unsigned long long		ft_un(t_print *print, va_list a)
{
	unsigned long long int		n;

	if (print->sz_fl == 1 || print->sz_fl == 2)
		n = print->sz_fl == 1 ? (unsigned char)va_arg(a, unsigned int) :
									(unsigned short)va_arg(a, unsigned int);
	else if (print->sz_fl == 3 || print->sz_fl == 4)
		n = print->sz_fl == 3 ? (unsigned long)va_arg(a, unsigned long int) :
					(unsigned long long)va_arg(a, unsigned long long int);
	else if (print->sz_fl == 5 || print->sz_fl == 6)
		n = print->sz_fl == 5 ? (uintmax_t)va_arg(a, uintmax_t) :
								(size_t)va_arg(a, size_t);
	else
		n = (unsigned int)va_arg(a, unsigned int);
	return (n);
}

char					*ft_s_tolower(char *t)
{
	int					i;

	i = -1;
	while (t[++i])
		if (t[i] >= 'A' && t[i] <= 'Z')
			t[i] += 32;
	return (t);
}
