/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_a_ch.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:45:00 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 13:47:26 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	ft_print_a_ch(t_print *print, va_list arguments, int *res)
{
	char	c;
	int		t;

	if (print->spec == 'c')
		c = print->sz_fl == 3 ? (char)va_arg(arguments, wint_t) :
									va_arg(arguments, int);
	else
		c = print->spec;
	if (print->width < 2)
		*res += write(1, &c, 1);
	else
	{
		t = print->width;
		if (print->min)
			*res += write(1, &c, 1);
		while (--t > 0)
			*res += print->null ? write(1, "0", 1) : write(1, " ", 1);
		if (!print->min)
			*res += write(1, &c, 1);
	}
}

void	ft_print_a_poiner(t_print *print, va_list arguments, int *res)
{
	char				*s;
	char				*t;
	unsigned long long	ui;
	int					i;

	ui = (unsigned long long)va_arg(arguments, void *);
	t = ft_ui_to_str(ui, 16);
	i = -1;
	while (t[++i])
		if (t[i] >= 'A' && t[i] <= 'Z')
			t[i] += 32;
	s = ft_strjoin("0x", t);
	free(t);
	if (!print->precis && !ui)
	{
		free(s);
		s = ft_strdup("0x");
	}
	s = ft_print_p_precis(print, s);
	s = ft_print_p_width(print, s);
	if (s)
		*res += write(1, s, ft_strlen(s));
	free(s);
}
