/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_a_num.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 13:35:58 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/18 13:40:50 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	ft_print_a_numbers(t_print *print, va_list a, int *res)
{
	char	*s;

	if (print->spec == 'u' || print->spec == 'i' || print->spec == 'd')
		s = print->spec == 'u' ? ft_print_u(print, a) : ft_print_id(print, a);
	else if (print->spec == 'o' || print->spec == 'x' || print->spec == 'X')
		s = print->spec == 'o' ? ft_print_o(print, a) : ft_print_x(print, a);
	else if (print->spec == 'b')
		s = ft_print_b(print, a);
	else
		s = ft_strdup("");
	if (s)
		*res += write(1, s, ft_strlen(s));
	free(s);
}

char	*ft_print_b(t_print *print, va_list arguments)
{
	char				*t;
	char				*s;
	unsigned long long	un;

	un = ft_un(print, arguments);
	t = ft_ui_to_str(un, 2);
	if (print->octothorp && t[0] != '0' && print->spec == 'b')
		ft_for_string("0b", t, 2);
	else
		s = t;
	if (!un && !print->precis)
	{
		free(s);
		s = ft_strdup("");
	}
	s = ft_print_p_precis(print, s);
	s = ft_print_p_width(print, s);
	return (s);
}

char	*ft_print_x(t_print *print, va_list arguments)
{
	char				*t;
	char				*s;
	unsigned long long	un;

	un = ft_un(print, arguments);
	t = ft_ui_to_str(un, 16);
	if (print->spec == 'x')
		t = ft_s_tolower(t);
	if (print->octothorp && t[0] != '0')
	{
		if (print->spec == 'x' || print->spec == 'X')
			s = print->spec == 'x' ? ft_for_string("0x", t, 2) :
									ft_for_string("0X", t, 2);
	}
	else
		s = t;
	if (!un && !print->precis)
	{
		free(s);
		s = ft_strdup("");
	}
	s = ft_print_p_precis(print, s);
	s = ft_print_p_width(print, s);
	return (s);
}

char	*ft_print_o(t_print *print, va_list arguments)
{
	char				*t;
	char				*s;
	unsigned long long	un;

	un = ft_un(print, arguments);
	t = ft_ui_to_str(un, 8);
	if (print->octothorp && t[0] != '0')
	{
		s = ft_strjoin("0", t);
		free(t);
	}
	else
		s = t;
	if (!un && !print->precis && !print->octothorp)
	{
		free(s);
		s = ft_strdup("");
	}
	s = ft_print_p_precis(print, s);
	s = ft_print_p_width(print, s);
	return (s);
}

char	*ft_print_u(t_print *print, va_list arguments)
{
	char				*s;
	unsigned long long	un;

	un = ft_un(print, arguments);
	s = ft_ui_to_str(un, 10);
	if (!un && !print->precis)
	{
		free(s);
		s = ft_strdup("");
	}
	s = ft_print_p_precis(print, s);
	s = ft_print_p_width(print, s);
	return (s);
}
